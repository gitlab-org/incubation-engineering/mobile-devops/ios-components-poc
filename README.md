# iOS Components POC


This project is a POC [CI/CD Catalog Component](https://docs.gitlab.com/ee/architecture/blueprints/ci_pipeline_components/) designed to perform build, sign, and release jobs for iOS applications using [Fastlane](http://fastlane.tools/).

**Please note:** this project is a proof of concept and should not be considered production-ready.

## Background

Building, signing, and releasing an iOS application is a fairly involved process. We leverage an open source tool called [Fastlane](http://fastlane.tools/) to make this process easier. Fastlane is written in Ruby, and can be installed via Rubygems or Homebrew. These components assume Fastlane is already installed, and that the jobs are being executed on a macOS Runner.

At the current time, Fastlane requires a specific folder structure in order to work correctly in a project. That structure expects a folder in the project root called `fastlane` and file in that folder called `Fastfile`. In these components, some work is done to ensure a `fastlane/Fastfile` is configured and written to the correct location in the project before the job is executed. This work is done in the first lines of the component script:

```yaml
- git clone https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/ios-components-poc.git
- ios-components/backup.sh
- mkdir fastlane
- erb destination="$[[ inputs.destination ]]" ... ios-components/ios_build_and_sign/Fastfile_template.erb > fastlane/Fastfile
```

1. Clone the `ios-components-poc` project to fetch the additional configuration files. This should be adjusted to be able to scope to a version or tag in the future.
1. Execute the `ios-components/backup.sh` which will backup the existing `fastlane` directory if one is present.
1. Create a new `fastlane` directory.
1. Use `erb` to build a `Fastfile` from `Fastfile_template.erb` and write it to `fastlane/Fastfile`

With the `Fastfile` in place, the last step is to simply kick of the job with `fastlane ios ios_components_build`

## Components

### `ios_build_and_sign`

The `build` component creates a signed build from the following inputs:

| Input               | Description |
|----------------------|----------------|
| `stage`              | The pipeline stage (default: `build`) | 
| `destination`     | Can be (`appstore`, `adhoc`,`enterprise` or `development`, default: `development`) |
| `app_identifier`  | The app's bundle identifier registered in the [Apple Developer Portal](https://developer.apple.com/account/resources/identifiers/list) |
| `project_name`    | The Xcode project name (i.e. `ios demo.xcodeproj`) |
| `build_schema`    | The project's build scheme |
| `build_configuration`| The Xcode build configuration |
| `build_export_method`| Method used to export the archive. Valid values are: `app-store`, `validation`, `ad-hoc`, `package`, `enterprise`, `development`, `developer-id` and `mac-application` |
| `output_directory` | Build output directory, default: `builds` |
| `output_name` | Build output file name, default: `build` |

This component expects that the required signing certificates have been imported to Secure Files in the GitLab project.

The build process creates an `ipa` artifact saved to `output_directory`/`output_name`.

###### Sample Component Configuration

```yaml
- component: 'gitlab.com/gitlab-org/incubation-engineering/mobile-devops/ios-components-poc/build@main'
  with:
    stage: build-app
    app_identifier: com.gitlab.ios-demo
    destination: appstore
    project_name: ios demo.xcodeproj
    build_schema: ios demo
    build_configuration: Release
    build_export_method: app-store
```


### `ios_release_to_testflight`

The `release_to_testflight` component takes an existing artifact (`.ipa` file), bumps the version, and uploads it to Test Flight.

| Input            	   | Description 	|
|----------------------|----------------|
| `stage`              | The pipeline stage (default: `release`) | 
| `app_identifier`  	| The app's bundle identifier registered in the [Apple Developer Portal](https://developer.apple.com/account/resources/identifiers/list) |
| `ipa_file`           | An existing buid artifact to be submitted to the App Store, default: `builds/build.ipa` |

This component expects that the Apple App Store integration has been enable for the GitLab project.

###### Sample Component Configuration

```yaml
- component: 'gitlab.com/gitlab-org/incubation-engineering/mobile-devops/ios-components-poc/release_to_testflight@main'
  with:
    stage: release-app
    app_identifier: com.gitlab.ios-demo
```

## Usage

Below is an example `.gitlab-ci.yml` file to show how these components can be used to create a complete build and release pipeline similar to the [iOS Demo](https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/demo-projects/ios-demo) project:

```yaml
include:
  - component: 'gitlab.com/gitlab-org/incubation-engineering/mobile-devops/ios-components-poc/ios_build_and_sign@main'
    with:
      stage: build-app
      app_identifier: com.gitlab.ios-demo
      destination: appstore
      project_name: ios demo.xcodeproj
      build_schema: ios demo
      build_configuration: Release
      build_export_method: app-store
  - component: 'gitlab.com/gitlab-org/incubation-engineering/mobile-devops/ios-components-poc/ios_release_to_testflight@main'
    with:
      stage: release-app
      app_identifier: com.gitlab.ios-demo

stages:
  - build-app
  - release-app
```
